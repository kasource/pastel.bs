--[[
	pastel_rectangle.bs
]]

-- parameter --
function setParam(v, ...)
	local t={...}
	return function()
		if type(v) == "table" then
			return unpack(v)
		else
			return v, unpack(t)
		end
	end
end

	pm={
		{"interval", 1, 100, 10},
		{"threshold", 0, 100, 0},
		{"density", 0, 100, 0},
		{"roughness", 0, 10, 0},
		{"flat", 0, 100, 0},
		{"angle", 0, 100, 0},
		{"soft_edge", 0, 100, 10},
		{"p_size", 0, 100, 25},
		{"p_density", 0, 100, 25},
		{"p_alpha", 0, 100, 25}
	}

	pm.ja={
		"間隔", "しきい値", "密度", "粗さ", "扁平率",
		"角度", "ソフトエッジ", "p_サイズ", "p_密度", "p_透明度"
	}

	for i in ipairs(pm) do

		local function eval(s, ...)
			assert(loadstring(string.format(s, ...)))()
		end

		if bs_lang() == "ja" then
			if pm.ja and pm.ja[i] then
				pm[i][1]=pm.ja[i]
			end
		end

		eval('param%d=setParam(pm[%d])', i, i)
		eval('pm[%d].v=bs_param%d()', i, i)

	end

	default_size=setParam(30, 0)
	firstDraw=true

	mixPressLv=8-6*(1-math.min(bs_width(), 20)/20)

-- sub routine --
function rgba(...)
	local t={...}
	local r, g, b, a=t[1] or 0, t[2] or 0, t[3] or 0, t[4] or bs_opaque()*255
	return {r=r, g=g, b=b, a=a}
end

function getColor(rate, a)
	local rate=rate or 0
	local t={bs_forebg(1-math.min(math.max(rate, 0), 1))}
	if a then
		return rgba(t[1], t[2], t[3], math.min(math.max(a, 0), 255))
	else
		return rgba(t[1], t[2], t[3])
	end
end

function getCanvasColor(x, y, radius)

	local pGet=rgba(0, 0, 0, 0)
	local result=rgba(0, 0, 0, 0)

	local moveAngle=bs_atan(bs_dir())
	local pickNum=math.max(90*math.min(bs_width(), 20)/20, 4)
	local pickAngle=180/pickNum

	for i=0, pickNum do

		local cx=x+radius*math.cos(moveAngle+math.rad(pickAngle*i-90))
		local cy=y+radius*math.sin(moveAngle+math.rad(pickAngle*i-90))

		pGet.a=bs_pixel_get_alpha(cx, cy)

		if pGet.a > 0 then
			pGet.r, pGet.g, pGet.b=bs_pixel_get(cx, cy)
			result.r=result.r+pGet.r*pGet.a
			result.g=result.g+pGet.g*pGet.a
			result.b=result.b+pGet.b*pGet.a
			result.a=result.a+pGet.a
		end
	end

	if result.a > 0 then
		result.r=math.floor(result.r/result.a)
		result.g=math.floor(result.g/result.a)
		result.b=math.floor(result.b/result.a)
		result.a=math.floor(result.a/(pickNum+1))
	end

	return result
end

-- main --
function main(x, y, p)

	local pp=1

	if pm[8].v > 0 then
		pp=(bs_width()/bs_width_max())^(8*pm[8].v/100)
	end

	local w=bs_width_max()*pp

	if firstDraw then
		lastX, lastY=x, y
	end

	local flat=1-pm[5].v/100

	local distance=bs_distance(lastX-x, lastY-y)
	local interval=w*pm[1].v/100*flat

	if not firstDraw then
		if distance < interval then
			return 0
		end
	end

	local dotUp=math.max(w-60, 0)/60
	local dot=math.min(math.max(w, 1), 3)+dotUp
	local iW, iH=math.ceil(w/dot), math.ceil(w/dot*flat)

	local foreColor=getColor(0)
	local canvasColor=getCanvasColor(x, y, w/2)

	if pm[10].v > 0 then
		foreColor.a=foreColor.a*(p^(8*pm[10].v/100))
	end

	if canvasColor.a == 0 then
		canvasColor=getColor(0, canvasColor.a)
	end

	if bs_opaque() <= 0.01 then
		foreColor=rgba(canvasColor.r, canvasColor.g, canvasColor.b)
		foreColor.a=(1-pm[10].v/100)*canvasColor.a*(p^2)
	end

	local wLimit=1

	if w < wLimit then
		foreColor.a=foreColor.a*(pp^((wLimit-w)/wLimit))
	end

	for ix=-iW/2, iW/2 do
		for iy=-iH/2, iH/2 do

			local tx=ix*dot+bs_grand(1+pm[4].v+dotUp)
			local ty=iy*dot+bs_grand(1+pm[4].v+dotUp)

			if (tx >= -w/2 and ty >= -w/2*flat) and (tx <= w/2 and ty <= w/2*flat) then
				if pm[2].v/100 < math.random() and (1-p^(6*pm[9].v/100)) < math.random() then

					local rad=bs_atan(bs_normal())+math.rad(180*pm[6].v/100)

					if pm[6].v == 100 then
						rad=rad+bs_grand(180)
					end

					local xx, yy=bs_rotate(tx, ty, rad)
					xx, yy=xx+x, yy+y

					local ww=dot*(1-pm[2].v/100+2*pm[3].v/100)*(p^(pm[9].v/100*(2-pm[4].v/10)))

					ww=ww*((1-math.max(math.abs(tx)/(w/2), math.abs(ty)/(w/2*flat)))^(pm[7].v/100))

					if ww < 1 then
						ww=1
					end

					local grid=math.random(1, 10)

					if math.floor(tx)%grid == 0 and math.floor(ty)%grid == 0 then
						ww=ww*((1+(3+math.max(w-50, 0)/250)*math.random()*pm[4].v/10)^math.random())
					end

					local mixRate=(((canvasColor.a/255)^0.1)*(1-p^(2+mixPressLv*(1-p))))^(bs_opaque()^0.5)

					local r=foreColor.r*(1-mixRate)+canvasColor.r*mixRate
					local g=foreColor.g*(1-mixRate)+canvasColor.g*mixRate
					local b=foreColor.b*(1-mixRate)+canvasColor.b*mixRate

					local a=(foreColor.a*math.random(p*100, 100)/100)

					bs_ellipse(xx, yy, ww, ww, 0, r, g, b, a)

				end
			end

		end
	end

	firstDraw=false
	lastX, lastY=x, y

	return 1
end
